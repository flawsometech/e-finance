package com.pas.efinance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Rashmi on 10/13/2017.
 */

public class Main_apdapter extends RecyclerView.Adapter<Main_apdapter.MyHolder1> {
    private Context context;
    private LayoutInflater inflater;
    private String[] mDataset;
   public Main_apdapter(Context context, String[] mDataset) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.mDataset = mDataset;
    }
    @Override
    public MyHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.today_collection_items, parent, false);
        return new Main_apdapter.MyHolder1(view);
    }

    @Override
    public void onBindViewHolder(MyHolder1 holder, int position) {
        // Get current position of item in RecyclerView to bind data and assign values from list
//        holder.Name.setText(mDataset[position]);
        holder.Name.setText(mDataset[0]);
        holder.Place.setText(mDataset[1]);
        holder.Date.setText(mDataset[2]);
        holder.Cash.setText(mDataset[3]);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "JST CLICKED", Toast.LENGTH_SHORT).show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    class MyHolder1 extends RecyclerView.ViewHolder {
        TextView Name;
        TextView Place;
        TextView Date;
        TextView Cash;
        public MyHolder1(View itemView) {
            super(itemView);
            Name = itemView.findViewById(R.id.txt_main_name);
            Place = itemView.findViewById(R.id.txt_main_place);
            Date =  itemView.findViewById(R.id.txt_main_date);
            Cash =  itemView.findViewById(R.id.txt_main_cash);
        }
    }
}
