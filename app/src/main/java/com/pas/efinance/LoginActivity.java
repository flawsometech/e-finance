package com.pas.efinance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.ed_login_ipaddress)
    EditText IpAddress;
    @BindView(R.id.ed_login_email)
    EditText email;
    @BindView(R.id.ed_login_password)
    EditText password;
    @BindView(R.id.ed_login_CollectorId)
    EditText CollectorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login)
//    public  void loginFirst(View v){
    public  void loginFirst(){
        String IpAddress1= IpAddress.getText().toString();
        String email1= email.getText().toString();
        String password1= password.getText().toString();
        String CollectorId1= CollectorId.getText().toString();
        if (!IpAddress1.isEmpty() && !email1.isEmpty()&& !password1.isEmpty()&& !CollectorId1.isEmpty()) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }
        else {


            IpAddress.setError("Please enter data");
            email.setError("Please enter data");
            password.setError("Please enter data");
            CollectorId.setError("Please enter data");
            Toast.makeText(getApplicationContext(),"Please enter All data!",Toast.LENGTH_SHORT).show();

        }

    }
}
